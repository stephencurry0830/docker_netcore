-- 导出 beta_user 的数据库结构
CREATE DATABASE IF NOT EXISTS `beta_user` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `beta_user`;

-- 导出  表 beta_user.Users 结构
CREATE TABLE IF NOT EXISTS `Users` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Company` varchar(255) DEFAULT NULL,
  `Name` varchar(50) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;



GRANT ALL PRIVILEGES ON `beta_user`.* TO 'heima'@'%' WITH GRANT OPTION;