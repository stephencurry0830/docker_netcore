﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using Dapper;
using MySql.Data.MySqlClient;

namespace User.Api.Data
{

    /// <summary>
    /// 数据库底层操作基类
    /// </summary>
    public class BaseRepository
    {
        //数据库连接类
        private readonly string _connectionString;

        /// <summary>
        /// 根据传入的数据库连接字符串实例化IDbConnection对象
        /// 子类需有带参构造函数，格式如:public ChildClass(string connectionString):base(connectionString){}
        /// </summary>
        /// <param name="connectionString"></param>
        public BaseRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        protected MySqlConnection GetAndOpenConnection()
        {
            var connection = new MySqlConnection(_connectionString);
            connection.Open();
            return connection;
        }

        #region 公共方法
        
        /// <summary>
        /// 是否存在
        /// </summary>
        /// <param name="sql">SELECT语句</param>
        /// <param name="param">查询参数值，传值示例 new {Param1 = param1, Param2 = param2, ...}</param>
        /// <param name="isProc">是否存储过程</param>
        /// <returns>存在返回True，否则返回False</returns>
        public bool Exists(string sql, object param, bool isProc = false)
        {
            using (var conntection = GetAndOpenConnection())
            {
                return conntection.ExecuteScalar<int>(sql, param,null,null, isProc ? CommandType.StoredProcedure : CommandType.Text) > 0;
            }
        }

        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="sql">INSERT语句</param>
        /// <param name="param">查询参数值，传值示例 new {Param1 = param1, Param2 = param2, ...}</param>
        /// <param name="isProc">是否存储过程</param>
        /// <returns>成功返回True，否则返回False</returns>
        public bool Add(string sql, object param, bool isProc = false)
        {
            return Execute(sql, param, isProc) > 0;
        }

        /// <summary>
        /// 添加
        /// </summary>
        /// <typeparam name="T">表对应的实体类</typeparam>
        /// <param name="sql">INSERT语句</param>
        /// <param name="t">实体Model</param>
        /// <param name="isProc">是否存储过程</param>
        /// <returns>成功返回True，否则返回False</returns>
        public bool Add<T>(string sql, T t, bool isProc = false) where T : class, new()
        {
            return Execute(sql, t, isProc) > 0;
        }

        /// <summary>
        /// 添加
        /// </summary>
        /// <typeparam name="T">表对应的实体类</typeparam>
        /// <param name="sql">INSERT语句</param>
        /// <param name="t">实体Model</param>
        /// <param name="isProc">是否存储过程</param>
        /// <returns>成功返回True，否则返回False</returns>
        public int Add_Identity<T>(string sql, T t, bool isProc = false) where T : class, new()
        {
            sql += ";SELECT LAST_INSERT_ID()";
            
            Object obj = GetSingle(sql, t, isProc);
            if (Object.Equals(obj, null) || Object.Equals(obj, DBNull.Value))
            {
                return 0;
            }
            return Convert.ToInt32(obj);
        }

        /// <summary>
        /// 单表批量添加
        /// </summary>
        /// <typeparam name="T">表对应的实体类</typeparam>
        /// <param name="sql">INSERT语句</param>
        /// <param name="list">实体Model List</param> 
        /// <returns>成功返回True，否则返回False</returns>
        public bool Add<T>(string sql, IList<T> list) where T : class, new()
        {
            return BulkExecute(sql, list) > 0;
        }

        /// <summary>
        /// 多表添加
        /// </summary>
        /// <param name="dictionary">第一个参数为SQL语句，第二个参数为实体Model</param>
        /// <returns>成功返回True，否则返回False</returns>
        public bool Add(IDictionary<string, object> dictionary)
        {
            return MultiExecute(dictionary) > 0;
        }

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="sql">UPDATE语句</param>
        /// <param name="param">查询参数值，传值示例 new {Param1 = param1, Param2 = param2, ...}</param> 
        /// <param name="isProc">是否存储过程</param>
        /// <returns>成功返回True，否则返回False</returns>
        public bool Update(string sql, object param, bool isProc = false)
        {
            return Execute(sql, param, isProc) > 0;
        }

        /// <summary>
        /// 更新
        /// </summary>
        /// <typeparam name="T">表对应的实体类</typeparam>
        /// <param name="sql">UPDATE语句</param>
        /// <param name="t">实体Model</param> 
        /// <param name="isProc">是否存储过程</param>
        /// <returns>成功返回True，否则返回False</returns>
        public bool Update<T>(string sql, T t, bool isProc = false) where T : class, new()
        {
            return Execute(sql, t, isProc) > 0;
        }

        /// <summary>
        /// 单表批量更新
        /// </summary>
        /// <typeparam name="T">表对应的实体类</typeparam>
        /// <param name="sql">UPDATE语句</param>
        /// <param name="list">实体Model List</param> 
        /// <returns>成功返回True，否则返回False</returns>
        public bool Update<T>(string sql, IList<T> list) where T : class, new()
        {
            return BulkExecute(sql, list) > 0;
        }

        /// <summary>
        /// 多表更新
        /// </summary>
        /// <param name="dictionary">第一个参数为SQL语句，第二个参数为实体Model</param>
        /// <returns>成功返回True，否则返回False</returns>
        public bool Update(IDictionary<string, object> dictionary)
        {
            return MultiExecute(dictionary) > 0;
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="sql">DELETE语句</param>
        /// <param name="param">查询参数值，传值示例 new {Param1 = param1, Param2 = param2, ...}</param>
        /// <param name="isProc">是否存储过程</param>
        /// <returns>成功返回True，否则返回False</returns>
        public bool Delete(string sql, object param, bool isProc = false)
        {
            return Execute(sql, param,isProc) > 0;
        }

        /// <summary>
        /// 单表批量删除
        /// </summary>
        /// <typeparam name="T">表对应的实体类</typeparam>
        /// <param name="sql">DELETE语句</param>
        /// <param name="list">实体Model List</param> 
        /// <returns>成功返回True，否则返回False</returns>
        public bool Delete<T>(string sql, IList<T> list) where T : class, new()
        {
            return BulkExecute(sql, list) > 0;
        }

        /// <summary>
        /// 多表删除
        /// </summary>
        /// <param name="dictionary">第一个参数为SQL语句，第二个参数为实体Model</param>
        /// <returns>成功返回True，否则返回False</returns>
        public bool Delete(IDictionary<string, object> dictionary)
        {
            return MultiExecute(dictionary) > 0;
        }

        /// <summary>
        /// 获取单个Model
        /// </summary>
        /// <typeparam name="T">表对应的实体类</typeparam>
        /// <param name="sql">SELECT语句</param>
        /// <param name="param">查询参数值，传值示例 new {Param1 = param1, Param2 = param2, ...}</param>
        /// <param name="isProc">是否存储过程</param>
        /// <returns>返回Model</returns>
        public T Get<T>(string sql, object param, bool isProc = false) where T : class, new()
        {
            using (var connection = GetAndOpenConnection())
            {
                return connection.QueryFirstOrDefault<T>(sql, param,null,null,isProc ? CommandType.StoredProcedure : CommandType.Text);
            }
        }

        /// <summary>
        /// 获取Model列表
        /// </summary>
        /// <typeparam name="T">表对应的实体类</typeparam>
        /// <param name="sql">SELECT语句</param>
        /// <param name="param">查询参数值，传值示例 new {Param1 = param1, Param2 = param2, ...}</param>
        /// <param name="isProc">是否存储过程</param>
        /// <returns>返回Model列表</returns>
        public List<T> GetList<T>(string sql, object param = null, bool isProc = false) where T : class, new()
        {
            using (var connection = GetAndOpenConnection())
            {
                return connection.Query<T>(sql, param,null,false,null,isProc ? CommandType.StoredProcedure : CommandType.Text).ToList();
            }
        }


        /// <summary>
        /// 查询数据(动态类型)
        /// </summary>
        /// <param name="sql">SELECT语句</param>
        /// <param name="param">查询参数值，传值示例 new {Param1 = param1, Param2 = param2, ...}</param>
        /// <param name="isProc">是否存储过程</param>
        /// <returns>返回动态类型</returns>
        public dynamic Get(string sql, object param = null, bool isProc = false)
        {
            using (var connection = GetAndOpenConnection())
            {
                return connection.QueryFirstOrDefault(sql, param,null,null, isProc ? CommandType.StoredProcedure : CommandType.Text);
            }
        }


        /// <summary>
        /// 获取列表(动态类型)
        /// </summary>
        /// <param name="sql">SELECT语句</param>
        /// <param name="param">查询参数值，传值示例 new {Param1 = param1, Param2 = param2, ...}</param>
        /// <param name="isProc">是否存储过程</param>
        /// <returns>返回动态类型</returns>
        public IEnumerable<dynamic> GetList(string sql, object param = null, bool isProc = false)
        {
            using (var connection = GetAndOpenConnection())
            {
                return connection.Query(sql, param,null,false,null,isProc ? CommandType.StoredProcedure : CommandType.Text);
            }
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        /// <param name="sql">SQL语句</param>
        /// <param name="param">查询参数值，传值示例 new {Param1 = param1, Param2 = param2, ...}</param>
        /// <returns></returns>
        public int Count(string sql, object param = null, bool isProc = false)
        {
            using (var connection = GetAndOpenConnection())
            {
                return connection.ExecuteScalar<int>(sql, param,null,null,isProc ? CommandType.StoredProcedure : CommandType.Text);
            }
        }


        /// <summary>
        /// 执行一条计算查询结果语句，返回查询结果（object）。
        /// </summary>
        /// <param name="sql">SQL语句</param>
        /// <param name="param">查询参数值，传值示例 new {Param1 = param1, Param2 = param2, ...}</param>
        /// /// <param name="isProc">是否存储过程</param>
        /// <returns>查询结果（object）</returns>
        public object GetSingle(string sql, object param = null, bool isProc = false)
        {
            using (var connection = GetAndOpenConnection())
            {
                return connection.ExecuteScalar<object>(sql, param, null, null, isProc ? CommandType.StoredProcedure : CommandType.Text);
            }
        }


        #endregion

        #region 数据工具方法


        public string ConvertToDbDate(object date)
        {
            return $"STR_TO_DATE('{date:yyyy-MM-dd HH:mm:ss}','%Y-%m-%d %H:%i:%s')";
        }
        
        #endregion

        #region 私有方法

        /// <summary>
        /// 单语句执行
        /// </summary>
        /// <param name="sql">SQL语句</param>
        /// <param name="param">查询参数值，传值示例 new {Param1 = param1, Param2 = param2, ...}</param>
        /// <param name="isProc">是否存储</param>
        /// <returns>返回受影响条数</returns>
        private int Execute(string sql, object param, bool isProc = false)
        {
            using (var connection = GetAndOpenConnection())
            {
                return connection.Execute(sql, param,null,null,isProc ? CommandType.StoredProcedure : CommandType.Text);
            }
        }

        /// <summary>
        /// 单表多语句执行
        /// </summary>
        /// <typeparam name="T">表对应的实体类</typeparam>
        /// <param name="sql">SQL语句</param>
        /// <param name="list">实体Model List</param>
        /// <returns>返回受影响的条数</returns>
        private int BulkExecute<T>(string sql, IList<T> list) where T : class, new()
        {
            using (var connection = GetAndOpenConnection())
            {
                int count = 0;
                using (var transaction = connection.BeginTransaction())
                {
                    try
                    {
                        count = connection.Execute(sql, list, transaction);
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                    }
                }
                return count;
            }
        }

        /// <summary>
        /// 多表多语句执行
        /// </summary>
        /// <param name="dictionary">string参数为SQL语句，object参数为实体Model</param>
        /// <returns>成功返回True，否则返回False</returns>
        private int MultiExecute(IDictionary<string, object> dictionary)
        {
            using (var connection = GetAndOpenConnection())
            {
                int count = 0;
                using (var transaction = connection.BeginTransaction())
                {
                    try
                    {
                        count += dictionary.Sum(keyValuePair => connection.Execute(keyValuePair.Key, keyValuePair.Value, transaction));
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                    }
                }
                return count;
            }
        }


        #endregion
    }
    

}
