﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using User.Api.Data;
using User.Api.Models;

namespace User.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private readonly BaseRepository _repository;
        public ValuesController(BaseRepository repository)
        {
            _repository = repository;
        }


        // GET api/values
        [HttpGet]
        public IActionResult Get()
        {
            var model = _repository.Get<UserModel>("select * from `Users` where name='jet'", null);
            return new JsonResult(model);
        }

    }
}
